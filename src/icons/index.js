import { ReactComponent as arrowDown } from './simple-arrow-down.svg';
import { ReactComponent as arrowUp } from './simple-arrow-up.svg';
import { ReactComponent as star } from './star.svg';
import { ReactComponent as starFilled } from './star-filled.svg';

const SvgMap = {
  arrowDown,
  arrowUp,
  star,
  starFilled,
};

export default SvgMap;
