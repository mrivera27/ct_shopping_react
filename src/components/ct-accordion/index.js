import React, { useEffect, useRef, useState } from 'react';
import cx from 'classname';
import { v4 as uuidv4 } from 'uuid';

import { Title, Icon } from '..';

const Accordion = props => {
  const {
    heading,
    headingLevel,
    iconAltText,
    opened,
    children,
    onOpenClose,
    borderColor,
  } = props;

  const [isOpen, setIsOpen] = useState(opened);
  const [maxHeight, setMaxHeight] = useState(0);
  const ref = useRef();
  const id = uuidv4();

  const block = 'ct-accordion';
  const openIcon = 'arrowDown';
  const closedIcon = 'arrowUp';

  useEffect(() => {
    if (ref.current) {
      setMaxHeight(ref.current.scrollHeight);
    }
  }, [ref]);

  const handleIsOpen = () => {
    onOpenClose && onOpenClose(!isOpen);
    setIsOpen(!isOpen);
  };

  return (
    <div className={cx(block)}>
      <div
        className={cx(`${block}__header`, {
          [`${block}__header--expanded`]: isOpen,
        })}
        style={{
          borderColor: isOpen ? borderColor : '#ccc',
        }}
      >
        <Title
          alignment='left'
          componentStyle='h2'
          headingLevel={headingLevel}
          text={heading}
        />
        {children && (
          <div className={cx(`${block}__action`)}>
            <Icon
              alt={iconAltText || heading}
              ariaControls={`sect${id}`}
              expanded={isOpen}
              id={`accordion${id}id`}
              imageDecorative={false}
              name={isOpen ? openIcon : closedIcon}
              onClick={handleIsOpen}
            />
          </div>
        )}
      </div>
      {children && (
        <div
          ref={ref}
          aria-hidden={!isOpen}
          aria-labelledby={`accordion${id}id`}
          className={cx(`${block}__content`, {
            [`${block}__content--expanded`]: isOpen,
          })}
          id={`sec${id}`}
          role='region'
          style={{ maxHeight: isOpen ? maxHeight : 0 }}
        >
          {children}
        </div>
      )}
    </div>
  );
};

Accordion.defaultProps = {
  opened: false,
  borderColor: '#000',
};

Accordion.displayName = 'Accordion';

export default Accordion;
