import React from 'react';
import cx from 'classname';

import SvgMap from '../../icons';

const toCamelCase = str => {
  return str.replace(/-([a-z])/g, g => g[1].toUpperCase());
};

const Icon = props => {
  const {
    alt,
    ariaControls,
    className,
    disabled,
    expanded,
    id,
    imageDecorative,
    isLink,
    name,
    onClick,
    style,
    target,
    url,
  } = props;

  let SvgIcon = null;

  if (name) {
    SvgIcon = SvgMap[toCamelCase(name)];
  }

  const TagName = isLink ? 'a' : onClick ? 'button' : 'div';
  const block = 'ct-icon';

  return (
    <TagName
      className={cx(block, {
        [`${block}--clickable`]: !!onClick || isLink,
        [className]: !!className,
      })}
      aria-controls={ariaControls}
      aria-disabled={disabled}
      aria-expanded={expanded}
      disabled={disabled}
      href={url || null}
      id={id}
      onClick={onClick}
      target={target || null}
    >
      {SvgIcon && (
        <SvgIcon
          style={{ ...style }}
          className={`${block}__svg`}
          aria-hidden='true'
          focusable='false'
        />
      )}
      {imageDecorative ? '' : <span className='sr-only'>{alt}</span>}
    </TagName>
  );
};

Icon.defaultProps = {
  disabled: false,
  imageDecorative: true,
  isLink: false,
};

Icon.displayName = 'Icon';

export default Icon;
