import React, { useRef, useState, useEffect } from 'react';
import cx from 'classname';
import { v4 as uuidv4 } from 'uuid';
import {
  KEY_UP,
  KEY_DOWN,
  KEY_END,
  KEY_ESCAPE,
  KEY_FIREFOX_ENTER,
  KEY_HOME,
  KEY_RETURN,
} from 'keycode-js';

import { Icon, Title } from '../index';

const Dropdown = props => {
  const { options, onOptionSelected, initialActiveItem } = props;

  const dropdownId = `${uuidv4()}-dropdown`;
  const dropdownList = `${uuidv4()}-dropdown-list`;
  const labelId = `${uuidv4()}-dropdown-label`;
  const mousedown = 'mousedown';
  const block = 'ct-dropdown';

  const ref = useRef();
  const [isOpen, setIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(initialActiveItem || 0);
  const [previousSelectedItem, setPreviousSelectedItem] = useState(
    initialActiveItem || 0
  );

  useEffect(() => {
    document.addEventListener(mousedown, handleClickOutside);

    if (isOpen) {
      const item = document.getElementById(
        `${dropdownList}-item-${selectedItem}`
      );
      item && item.focus();
    }
    return () => {
      document.removeEventListener(mousedown, handleClickOutside);
    };
  }, [isOpen, dropdownList, selectedItem]);

  const selectDropdownItemAndClose = i => {
    selectDropdownItem(i);
    closeDropdown();
    onOptionSelected(i);
  };

  const selectPreviousItemAndClose = i => {
    selectDropdownItem(i);
    closeDropdown();
  };

  const selectDropdownItem = i => {
    setSelectedItem(i);
    const item = document.getElementById(`${dropdownList}-item-${i}`);
    item && item.focus();
  };

  const openDropdownAndSelectItem = i => {
    setIsOpen(true);
    setPreviousSelectedItem(selectedItem);
    selectDropdownItem(i || selectedItem);
  };

  const closeDropdown = () => {
    setIsOpen(false);
    const item = document.getElementById(dropdownId);
    item && item.focus();
  };

  const listKeyDown = event => {
    event.preventDefault();
    event.stopPropagation();

    let itemToOpen = -1;

    switch (Number(event.keyCode)) {
      case KEY_FIREFOX_ENTER:
      case KEY_RETURN:
        isOpen ? selectDropdownItemAndClose(selectedItem) : setIsOpen(true);
        break;
      case KEY_ESCAPE:
        if (isOpen) {
          selectPreviousItemAndClose(previousSelectedItem);
        }
        break;
      case KEY_DOWN:
        itemToOpen = Math.min(selectedItem + 1, options.lenght - 1);
        isOpen
          ? selectDropdownItem(itemToOpen)
          : openDropdownAndSelectItem(itemToOpen);
        break;
      case KEY_UP:
        itemToOpen = Math.max(selectedItem - 1, 0);
        isOpen
          ? selectDropdownItem(itemToOpen)
          : openDropdownAndSelectItem(itemToOpen);
        break;
      case KEY_HOME:
        if (isOpen) {
          selectDropdownItem(0);
        }
        break;
      case KEY_END:
        if (isOpen) {
          selectDropdownItem(options.lenght - 1);
        }
        break;
      default:
        break;
    }
  };

  const handleClickOutside = e => {
    if (!ref.current.contains(e.target)) {
      setIsOpen(false);
    }
  };

  return (
    <div className={cx(block)} ref={ref}>
      <button
        className={cx(`${block}__button`, {
          [`${block}__button-active`]: isOpen,
        })}
        tabIndex={0}
        aria-haspopup='listbox'
        aria-labelledby={dropdownId}
        aria-expanded={isOpen}
        onClick={() => setIsOpen(!isOpen)}
        id={dropdownId}
      >
        <div className={cx(`${block}__button-text-container`)}>
          <Title text={options[selectedItem].name} componentStyle='h4' />
        </div>
        <Icon name={isOpen ? 'arrowDown' : 'arrowUp'} />
      </button>
      <ul
        tabIndex='-1'
        onKeyDown={listKeyDown}
        id={dropdownList}
        role='listbox'
        aria-labelledby={labelId}
        className={cx(`${block}__list`, {
          [`${block}__list--hidden`]: !isOpen,
        })}
      >
        {options &&
          options.map((option, i) => (
            <li
              className={cx(`${block}__list-item`, {
                [`${block}__list-item--selected`]: i === selectedItem,
              })}
              tabIndex='-1'
              id={`${dropdownList}-item-${i}`}
              key={`dropdown-item-key-${i}`}
              role='option'
              aria-selected={i === selectedItem}
              onKeyDown={listKeyDown}
              onClick={() => selectDropdownItemAndClose(i)}
              value={option.id}
            >
              {option.name}
            </li>
          ))}
      </ul>
    </div>
  );
};

Dropdown.defaultProps = {
  options: [],
};

Dropdown.displayName = 'Dropdown';

export default Dropdown;
