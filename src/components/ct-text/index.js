import React from 'react';
import cx from 'classname';

const Text = props => {
  const { p, text, alignment, size, className, type, color, id, children } =
    props;

  const TagName = p ? 'p' : 'span';
  const content = text || children;
  const block = 'ct-text';

  return (
    <TagName
      className={cx(
        block,
        `${block}--alignment-${alignment}`,
        `${block}--size-${size}`,
        `${block}--type-${type}`,
        `${block}--color-${color}`,
        { [className]: !!className }
      )}
      id={id}
    >
      {content}
    </TagName>
  );
};

Text.defaultProps = {
  alignment: 'left',
  size: 'default',
  color: 'mine',
};

Text.displayName = 'Text';

export default Text;
