import React from 'react';
import cx from 'classname';

const Switch = props => {
  const { label, checked, onChange } = props;

  const block = 'ct-switch';

  return (
    <div className={cx(block)}>
      {label}
      <label className={cx(`${block}__wrapper`)}>
        <input
          type='checkbox'
          className={cx(`${block}__input`)}
          checked={checked}
          onChange={onChange}
        />
        <span className={cx(`${block}__slider`)} />
      </label>
    </div>
  );
};

Switch.defaultProps = {
  label: '',
};

Switch.displayName = 'Switch';

export default Switch;
