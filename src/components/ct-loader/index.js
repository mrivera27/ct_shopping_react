import React, { Suspense } from 'react';
import cx from 'classname';
import PropTypes from 'prop-types';

const Loader = props => {
  const { children } = props;

  const block = 'ct-loader';

  return (
    <Suspense fallback={<div className={cx(block)} />}>{children}</Suspense>
  );
};

Loader.defaultProps = {};

Loader.propTypes = {
  children: PropTypes.object,
};

Loader.displayName = 'Loader';

export default Loader;
