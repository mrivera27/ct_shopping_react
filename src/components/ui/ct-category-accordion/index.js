import React, { useEffect, useState } from 'react';
import cx from 'classname';
import Swal from 'sweetalert2';

import { Accordion, Text, Card } from '../../index';
import { Dropdown } from '../../index';
import Firebase from '../../../libs/services/Firebase';
import toast from 'react-hot-toast';

const CategoryAccordion = props => {
  const { category, items } = props;

  const [rearrangeItems, setRearrangeItems] = useState([...items]);

  const hasItems = items.length > 0;
  const block = 'ct-category-accordion';
  const rearrangedOptions = [{ name: 'A to Z' }, { name: 'Z to A' }];

  useEffect(() => {
    setRearrangeItems([...items]);
  }, [items]);

  const sortByName = (a, b) => {
    if (a < b) {
      return -1;
    }

    if (a > b) {
      return 1;
    }

    return 0;
  };

  const handleRearrangeItems = i => {
    let sortedList = [...items];

    switch (i) {
      case 0:
        sortedList = sortedList.sort((a, b) =>
          sortByName(a.name.toLowerCase(), b.name.toLowerCase())
        );
        break;
      case 1:
        sortedList = sortedList.sort((a, b) =>
          sortByName(b.name.toLowerCase(), a.name.toLowerCase())
        );
        break;
      default:
        console.log("There's no option that matches your selection.");
        break;
    }

    setRearrangeItems(sortedList);
  };

  const handleOnCategoryDelete = () => {
    Swal.fire({
      title: 'Delete category?',
      text: 'This category will be permanently deleted.',
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
      confirmButtonColor: '#1774e5',
    }).then(result => {
      if (result.isConfirmed) {
        Firebase.remove('categories', category.id)
          .then(() => window.location.reload())
          .catch(() =>
            toast.error(
              'Something went wrong when tried to delete the category'
            )
          );
      }
    });
  };

  const handleOnItemDelete = id => {
    Swal.fire({
      title: 'Delete item?',
      text: 'This item will be permanently deleted.',
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
      confirmButtonColor: '#1774e5',
    }).then(result => {
      if (result.isConfirmed) {
        Firebase.remove('items', id)
          .then(() => window.location.reload())
          .catch(() =>
            toast.error('Something went wrong when tried to delete the item')
          );
      }
    });
  };

  return (
    <Accordion
      heading={category.name}
      opened={hasItems}
      borderColor={category.color}
    >
      <div className={cx(`${block}__options-container`)}>
        <button
          className={cx(`${block}__delete-button`)}
          onClick={handleOnCategoryDelete}
        >
          Delete category
        </button>
        {hasItems && (
          <Dropdown
            options={rearrangedOptions}
            onOptionSelected={handleRearrangeItems}
          />
        )}
      </div>
      <div className={cx(`${block}__content-grid`)}>
        {hasItems ? (
          rearrangeItems.map(item => (
            <Card
              key={item.id}
              heading={item.name}
              fileReference={item.image}
              imageAltText={`${item.name}'s picture`}
              onStarred={() => {}}
            >
              <button
                className={cx(
                  `${block}__delete-button`,
                  `${block}__delete-button--sm`
                )}
                onClick={() => handleOnItemDelete(item.id)}
              >
                Delete
              </button>
            </Card>
          ))
        ) : (
          <Text text='This category has no items' />
        )}
      </div>
    </Accordion>
  );
};

export default CategoryAccordion;
