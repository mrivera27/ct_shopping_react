import React from 'react';
import cx from 'classname';

import { TopNav } from '../index';

const MainLayout = props => {
  const { children } = props;

  const block = 'ct-main-layout';

  return (
    <>
      <TopNav />
      <main className={cx(block)}>
        <div className={cx(`${block}--container`)}>{children}</div>
      </main>
      <footer className={cx(`${block}__footer`)} />
    </>
  );
};

MainLayout.displayName = 'MainLayout';

export default MainLayout;
