import React, { useEffect } from 'react';
import cx from 'classname';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import toast from 'react-hot-toast';

import { APP_PATHS } from '../../../../libs/constants';
import { userSelector, clearState } from '../../../../features/user/slice';
import { signoutUser } from '../../../../features/user/api';

const TopNav = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isSuccess, isError, errorMessage, email } = useSelector(userSelector);

  const block = 'ct-top-nav';

  useEffect(() => {
    return () => {
      dispatch(clearState());
    };
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      toast.error(errorMessage);
      dispatch(clearState());
    }
    if (isSuccess) {
      dispatch(clearState());
      navigate(APP_PATHS.signin);
    }
  }, [dispatch, errorMessage, isError, isSuccess, navigate]);

  const handleOnSignOut = () => {
    dispatch(signoutUser());
  };

  return (
    <header className={cx(block)}>
      <div className={cx(`${block}__item`, `${block}__item--left`)}>
        <Link to={APP_PATHS.root} className={cx(`${block}__logo`)}>
          [CT]
        </Link>
      </div>
      <div className={cx(`${block}__item`, `${block}__item--center`)}>
        <Link className={cx(`${block}__link`)} to={APP_PATHS.root}>
          Home
        </Link>
        <Link className={cx(`${block}__link`)} to={APP_PATHS.create}>
          Create
        </Link>
        <Link className={cx(`${block}__link`)} to={APP_PATHS.favorites}>
          Favorites
        </Link>
      </div>
      <div className={cx(`${block}__item`, `${block}__item--right`)}>
        <span className={cx(`${block}__user-email`)}>{email}</span>(
        <button onClick={handleOnSignOut} className={cx(`${block}__link`)}>
          Sign Out
        </button>
        )
      </div>
    </header>
  );
};

TopNav.displayName = 'TopNav';

export default TopNav;
