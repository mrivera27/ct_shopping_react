import React from 'react';
import cx from 'classname';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';

import { Title, Text } from '../../../index';
import { userSelector } from '../../../../features/user/slice';

const AuthForm = props => {
  const { title, description, linkTo, linkText, footerDescription, onSubmit } =
    props;

  const { isFetching } = useSelector(userSelector);
  const { register, handleSubmit } = useForm();

  const block = 'ct-auth';

  return (
    <form className={cx(block)} onSubmit={handleSubmit(onSubmit)}>
      <Title
        alignment='center'
        text='[CT]'
        className={cx(`${block}__logo`)}
        headingLevel='h1'
      />

      <div className={cx(`${block}__heading`)}>
        <Title text={title} />
        <Text text={description} />
      </div>

      <div className={cx(`${block}__wrapper`)}>
        <label htmlFor='email'>Email</label>
        <input
          type='email'
          autoComplete='email'
          {...register('email', {
            required: true,
            patter: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/i,
          })}
          required
        />
        <label htmlFor='password'>Password</label>
        <input
          type='password'
          autoComplete='current-password'
          {...register('password', {
            required: true,
          })}
          required
        />
        <button
          type='submit'
          disabled={isFetching}
          className={cx({ 'ct-disabled': isFetching })}
        >
          {isFetching ? 'Loading...' : title}
        </button>
      </div>

      <div className={cx(`${block}__footer`)}>
        <Text text={footerDescription} />
        <Link className={cx(`${block}__link`)} to={linkTo}>
          {linkText}
        </Link>
      </div>
    </form>
  );
};

AuthForm.defaultProps = {};

AuthForm.displayName = 'AuthForm';

export default AuthForm;
