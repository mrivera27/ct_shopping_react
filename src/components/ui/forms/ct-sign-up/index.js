import React from 'react';
import cx from 'classname';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';

import { Title, Text } from '../../../index';
import { APP_PATHS } from '../../../../libs/constants';
import { userSelector } from '../../../../features/user/slice';

const SignUpForm = props => {
  const { onSubmit } = props;

  const { isFetching } = useSelector(userSelector);
  const { register, handleSubmit } = useForm();

  const block = 'ct-sign-up';

  return (
    <form className={cx(block)} onSubmit={handleSubmit(onSubmit)}>
      <Title
        alignment='center'
        text='[CT]'
        className={cx(`${block}__logo`)}
        headingLevel='h1'
      />
      <div className={cx(`${block}__heading`)}>
        <Title text='Sign Up' />
        <Text text='To sign up please enter your name, email, and password' />
      </div>
      <div className={cx(`${block}__wrapper`)}>
        <label htmlFor='name'>Name</label>
        <input
          type='text'
          autoComplete='name'
          {...register('name', {
            required: true,
          })}
          required
        />
        <label htmlFor='email'>Email</label>
        <input
          type='email'
          autoComplete='email'
          {...register('email', {
            required: true,
            patter: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/i,
          })}
          required
        />
        <label htmlFor='password'>Password</label>
        <input
          type='password'
          autoComplete='current-password'
          {...register('password', {
            required: true,
          })}
          required
        />
        <button type='submit'>{isFetching ? 'Loading...' : 'Sign in'}</button>
      </div>
      <div className={cx(`${block}__footer`)}>
        <Text text='Already have an account?' />
        <Link className={cx(`${block}__link`)} to={APP_PATHS.signin}>
          Sign In
        </Link>
      </div>
    </form>
  );
};

SignUpForm.defaultProps = {};

SignUpForm.displayName = 'SignUpForm';

export default SignUpForm;
