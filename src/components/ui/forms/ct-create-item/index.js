import React, { useEffect, useState } from 'react';
import cx from 'classname';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';

import { Title, Text, Dropdown } from '../../../index';
import Firebase from '../../../../libs/services/Firebase';
import FileUpload from '../../../ct-file-upload';
import { useNavigate } from 'react-router-dom';
import { APP_PATHS } from '../../../../libs/constants';

const CreateItemForm = props => {
  const { isDuplicated, setCreated, categories, switchElement } = props;

  const { register, handleSubmit } = useForm();
  const [categoryId, setCategoryId] = useState('');
  const [image, setImage] = useState('');
  const [isSubmiting, setIsSubmiting] = useState(false);
  const navigate = useNavigate();

  const block = 'ct-create-item';

  useEffect(() => {
    const category = categories[0];
    setCategoryId(category.id);
  }, [categories]);

  const handleOnCategoryChange = i => {
    setCategoryId(categories[i].id);
  };

  const handleOnImageChange = file => {
    setImage(file);
  };

  const onSubmit = data => {
    setIsSubmiting(true);

    if (isDuplicated(data.name)) {
      toast.error('Item already exists');
      setCreated(false);
      setIsSubmiting(false);
    } else {
      Firebase.uploadFile(image, 'items')
        .then(fileUrl => {
          Firebase.add('items', {
            name: data.name,
            category_id: categoryId,
            image: fileUrl,
          })
            .then(() => {
              setIsSubmiting(false);
              setCreated(true);
              toast.success('Item succesfully created');
              navigate(APP_PATHS.root);
            })
            .catch(() => {
              toast.error('Something went wrong when tried to create the item');
              setCreated(false);
              setIsSubmiting(false);
            });
        })
        .catch(() => {
          toast.error('Something went wrong when tried to upload the image');
          setCreated(false);
          setIsSubmiting(false);
        });
    }
  };

  return (
    <form className={cx(block)} onSubmit={handleSubmit(onSubmit)}>
      <div className={cx(`${block}__heading`)}>
        <Title text='Create item' />
        <span className={cx(`${block}__heading-switch`)}>{switchElement}</span>
      </div>

      <div className={cx(`${block}__wrapper`)}>
        <label htmlFor='name'>
          <Text text='Name' />
        </label>
        <input
          type='text'
          name='name'
          {...register('name', { required: true })}
          required
        />
        <label htmlFor='category'>
          <Text text='Category' />
        </label>
        <Dropdown
          options={categories}
          onOptionSelected={handleOnCategoryChange}
        />
        <label htmlFor='image'>
          <Text text='Image' />
        </label>
        <FileUpload handleFile={handleOnImageChange} />
        <button
          disabled={isSubmiting}
          className={cx({ 'ct-disabled': isSubmiting })}
          type='submit'
        >
          {isSubmiting ? 'Loading...' : 'Create'}
        </button>
      </div>
    </form>
  );
};

CreateItemForm.defaultProps = {};

export default CreateItemForm;
