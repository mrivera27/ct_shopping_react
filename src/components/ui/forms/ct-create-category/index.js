import React, { useState } from 'react';
import cx from 'classname';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';

import { Title, Text } from '../../../index';
import Firebase from '../../../../libs/services/Firebase';

const CreateCategoryForm = props => {
  const { isDuplicated, setCreated, switchElement } = props;

  const { register, handleSubmit } = useForm();
  const [isSubmiting, setIsSubmiting] = useState(false);

  const block = 'ct-create-category';

  const onSubmit = data => {
    setIsSubmiting(true);

    if (isDuplicated(data.name)) {
      toast.error('Category already exists');
      setCreated(false);
      setIsSubmiting(false);
    } else {
      Firebase.add('categories', { name: data.name, color: data.color })
        .then(() => {
          setIsSubmiting(false);
          setCreated(true);
          toast.success('Category succesfully created');
        })
        .catch(() => {
          toast.error('Something went wrong when tried to create the category');
          setCreated(false);
          setIsSubmiting(false);
        });
    }
  };

  return (
    <form className={cx(block)} onSubmit={handleSubmit(onSubmit)}>
      <div className={cx(`${block}__heading`)}>
        <Title text='Create category' />
        <span className={cx(`${block}__heading-switch`)}>{switchElement}</span>
      </div>

      <div className={cx(`${block}__wrapper`)}>
        <label htmlFor='name'>
          <Text text='Name' size='large' />
        </label>
        <input type='text' {...register('name', { required: true })} required />
        <label htmlFor='color'>
          <Text text='Color' />
        </label>
        <input
          type='color'
          {...register('color', { required: true })}
          required
        />
        <button
          disabled={isSubmiting}
          className={cx({ 'ct-disabled': isSubmiting })}
          type='submit'
        >
          {isSubmiting ? 'Loading...' : 'Create'}
        </button>
      </div>
    </form>
  );
};

CreateCategoryForm.defaultProps = {};

export default CreateCategoryForm;
