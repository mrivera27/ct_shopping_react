import React, { useState } from 'react';
import cx from 'classname';

import { Title, Icon } from '../index';

const Card = props => {
  const {
    children,
    heading,
    headingLevel,
    imageAltText,
    fileReference,
    className,
    onStarred,
  } = props;

  const [starred] = useState(false);

  const block = 'ct-card';

  const handleOnStarred = () => {
    onStarred();
  };

  return (
    <div className={cx(block)}>
      {fileReference && (
        <div
          className={cx(`${block}__image`, className)}
          role='img'
          aria-hidden={!imageAltText}
          aria-label={imageAltText}
          style={{ backgroundImage: `url("${fileReference}")` }}
        />
      )}
      <div className={cx(`${block}__body`)}>
        <Title
          className={cx(`${block}__heading`)}
          headingLevel={headingLevel}
          componentStyle='h4'
        >
          <span>{heading}</span>
          <Icon
            className={cx(`${block}__star-icon`)}
            alt={`${starred ? 'Unstarred' : 'Starred'} item`}
            imageDecorative={false}
            name={starred ? 'starFilled' : 'star'}
            onClick={handleOnStarred}
          />
        </Title>
        {children}
      </div>
    </div>
  );
};

Card.defaultProps = {};

Card.displayName = 'Card';

export default Card;
