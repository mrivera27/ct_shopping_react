import React from 'react';
import cx from 'classname';

const Title = props => {
  const {
    alignment,
    children,
    className,
    headingLevel,
    id,
    removeUppercase,
    tabIndex,
    text,
  } = props;

  const TagName = headingLevel;
  const content = text || children;
  const block = 'ct-title';
  const componentStyle =
    props.componentStyle || (headingLevel === 'div' ? 'h1' : headingLevel);

  return (
    <TagName
      className={cx(
        block,
        `${block}--alignment-${alignment}`,
        `${block}--style-${componentStyle}`,
        {
          [`${block}--remove-uppercase`]: removeUppercase,
          [className]: !!className,
        }
      )}
      id={id}
      tabIndex={tabIndex}
    >
      {content}
    </TagName>
  );
};

Title.defaultProps = {
  headingLevel: 'h2',
  alignment: 'left',
  text: '',
  componentStyle: null,
  tabIndex: -1,
};

Title.displayName = 'Title';

export default Title;
