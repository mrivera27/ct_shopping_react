import React from 'react';
import cx from 'classname';

const SearchBar = props => {
  const { onChange } = props;

  const block = 'ct-search-bar';

  return (
    <div className={cx(block)}>
      <input
        className={cx(`${block}__input`)}
        type='text'
        placeholder='Search'
        onChange={onChange}
      />
    </div>
  );
};

SearchBar.defaultProps = {};

SearchBar.displayName = 'SearchBar';

export default SearchBar;
