import React, { useRef, useState } from 'react';
import cx from 'classname';

import { Text } from '../index';

const FileUpload = props => {
  const { handleFile } = props;

  const hiddenFileInput = useRef(null);
  const [file, setFile] = useState();

  const block = 'ct-file-upload';

  const handleClick = () => {
    hiddenFileInput.current.click();
  };
  const handleChange = event => {
    event.preventDefault();
    event.stopPropagation();
    const fileUploaded = event.target.files[0];
    setFile(fileUploaded);
    handleFile(fileUploaded);
  };
  return (
    <div className={cx(block)}>
      <button
        type='button'
        className={cx(`${block}__button`, 'ct-button')}
        onClick={handleClick}
      >
        Upload
      </button>
      {file && <Text className={cx(`${block}__filename`)} text={file.name} />}
      <input
        className={cx(`${block}__input`)}
        type='file'
        ref={hiddenFileInput}
        onChange={handleChange}
      />
    </div>
  );
};
export default FileUpload;
