import Cookies from 'js-cookie';
import { createAsyncThunk } from '@reduxjs/toolkit';

import Firebase from '../../libs/services/Firebase';
import { AUTH_COOKIE_KEY, USER_STORAGE_KEY } from '../../libs/constants';

export const getAuthToken = () => {
  try {
    return Cookies.get(AUTH_COOKIE_KEY);
  } catch (error) {
    console.log('>>> src/feature/auth/api.js: getAuthToken -> error', error);
    return '';
  }
};

export const getCurrentUser = () => {
  try {
    return localStorage.getItem(USER_STORAGE_KEY);
  } catch (error) {
    console.log('>>> src/feature/auth/api.js: getCurrentUser -> error', error);
    return '';
  }
};

const setAuthToken = token => {
  try {
    token
      ? Cookies.set(AUTH_COOKIE_KEY, token)
      : Cookies.remove(AUTH_COOKIE_KEY);
  } catch (error) {
    console.log('>>> src/feature/auth/api.js: setAuthToken -> error', error);
  }
};

const setCurrentUser = user => {
  try {
    user
      ? localStorage.setItem(USER_STORAGE_KEY, user)
      : localStorage.removeItem(USER_STORAGE_KEY);
  } catch (error) {
    console.log('>>> src/feature/auth/api.js: setCurrentUser -> error', error);
  }
};

export const signinUser = createAsyncThunk(
  'users/signinUser',
  async ({ email, password }, thunkAPI) => {
    const message = 'Something went wrong when tried to sign in';

    try {
      const response = await Firebase.signin(email, password);

      if (response.user) {
        const { uid, email } = response.user;

        setAuthToken(uid);
        setCurrentUser(email);

        return { email: email, auth: uid };
      } else {
        return thunkAPI.rejectWithValue({ message: message });
      }
    } catch (e) {
      return thunkAPI.rejectWithValue({ message: message });
    }
  }
);

export const signupUser = createAsyncThunk(
  'users/signupUser',
  async ({ email, password }, thunkAPI) => {
    const message = 'Something went wrong when tried to sign up';

    try {
      const response = await Firebase.signup(email, password);
      if (response.user) {
        const { uid, email } = response.user;

        setAuthToken(uid);
        setCurrentUser(email);

        return { email: email, auth: uid };
      } else {
        return thunkAPI.rejectWithValue({ message: message });
      }
    } catch (e) {
      return thunkAPI.rejectWithValue({ message: message });
    }
  }
);

export const signoutUser = createAsyncThunk(
  'users/signoutUser',
  async (_, thunkAPI) => {
    const message = 'Something went wrong when tried to sign out';

    try {
      await Firebase.signout();

      setAuthToken();
      setCurrentUser();
    } catch (e) {
      return thunkAPI.rejectWithValue({ message: message });
    }
  }
);
