import { createSlice } from '@reduxjs/toolkit';

import {
  getCurrentUser,
  getAuthToken,
  signinUser,
  signupUser,
  signoutUser,
} from './api';

const initialState = {
  email: getCurrentUser(),
  auth: getAuthToken(),
  isFetching: false,
  isSuccess: false,
  isError: false,
  errorMessage: '',
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    clearState: state => {
      state.isError = false;
      state.isSuccess = false;
      state.isFetching = false;

      return state;
    },
  },
  extraReducers: {
    [signupUser.fulfilled]: (state, { payload }) => {
      state.isFetching = false;
      state.isSuccess = true;
      state.auth = payload.auth;
      state.email = payload.email;
    },
    [signupUser.pending]: state => {
      state.isFetching = true;
    },
    [signupUser.rejected]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [signinUser.fulfilled]: (state, { payload }) => {
      state.auth = payload.auth;
      state.email = payload.email;
      state.isFetching = false;
      state.isSuccess = true;
      return state;
    },
    [signinUser.rejected]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [signinUser.pending]: state => {
      state.isFetching = true;
    },
    [signoutUser.fulfilled]: state => {
      state.auth = '';
      state.email = '';
      state.isFetching = false;
      state.isSuccess = true;
      return state;
    },
    [signoutUser.rejected]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [signoutUser.pending]: state => {
      state.isFetching = true;
    },
  },
});

export const { clearState } = userSlice.actions;

export const userSelector = state => state.user;
