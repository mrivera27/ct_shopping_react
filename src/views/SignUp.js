import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import toast from 'react-hot-toast';
import { useNavigate, Navigate } from 'react-router-dom';

import { AuthForm } from '../components/ui/forms';
import { userSelector, clearState } from '../features/user/slice';
import { signupUser } from '../features/user/api';
import { APP_PATHS } from '../libs/constants';

const SignUp = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isSuccess, isError, errorMessage, auth } = useSelector(userSelector);

  useEffect(() => {
    return () => {
      dispatch(clearState());
    };
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      toast.error(errorMessage);
      dispatch(clearState());
    }
    if (isSuccess) {
      dispatch(clearState());
      navigate(APP_PATHS.root);
    }
  }, [dispatch, errorMessage, isError, isSuccess, navigate]);

  const handleOnSignUp = data => {
    dispatch(signupUser(data));
  };

  return (
    <>
      {auth ? (
        <Navigate replace to={APP_PATHS.root} />
      ) : (
        <AuthForm
          title='Sign Up'
          description='To sign up please enter your email and password'
          linkTo={APP_PATHS.signin}
          linkText='Sign In'
          footerDescription='Already have an account?'
          onSubmit={handleOnSignUp}
        />
      )}
    </>
  );
};

export default SignUp;
