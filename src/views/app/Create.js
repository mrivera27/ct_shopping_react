import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

import Firebase from '../../libs/services/Firebase';
import { CreateCategoryForm, CreateItemForm } from '../../components/ui/forms';
import { Switch } from '../../components';

const getData = (path, callBack) => {
  Firebase.getAll(path).then(snapshot => {
    const docs = [];

    snapshot.forEach(doc => {
      docs.push({
        id: doc.id,
        ...doc.data(),
      });
    });

    callBack(docs);
  });
};

const Create = () => {
  const [categories, setCategories] = useState([]);
  const [items, setItems] = useState([]);
  const [created, setCreated] = useState(false);
  const [switchChecked, setSwitchChecked] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();

  useEffect(() => {
    getData('categories', setCategories);
    getData('items', setItems);
  }, [created]);

  useEffect(() => {
    setSwitchChecked(searchParams.get('section') === 'item');
  }, [searchParams]);

  const checkCategoryExists = name => {
    return !!categories.find(
      category => category.name.toLowerCase() === name.toLowerCase()
    );
  };

  const checkItemExists = name => {
    return !!items.find(item => item.name.toLowerCase() === name.toLowerCase());
  };

  const handleOnSwitchChange = event => {
    const isChecked = event.target.checked;
    setSwitchChecked(isChecked);
    setSearchParams({ section: isChecked ? 'item' : 'category' });
  };

  const CreateSwitch = (
    <Switch checked={switchChecked} onChange={handleOnSwitchChange} />
  );

  return (
    <>
      {switchChecked ? (
        categories.length >= 1 && (
          <CreateItemForm
            isDuplicated={checkItemExists}
            setCreated={setCreated}
            categories={categories}
            switchElement={CreateSwitch}
          />
        )
      ) : (
        <CreateCategoryForm
          isDuplicated={checkCategoryExists}
          setCreated={setCreated}
          switchElement={CreateSwitch}
        />
      )}
    </>
  );
};

export default Create;
