import React, { useState, useEffect } from 'react';

import { SearchBar, Text } from '../../components';
import { getData } from '../../libs/services/API';
import { groupByKey } from '../../libs/helpers/collections';
import CategoryAccordion from '../../components/ui/ct-category-accordion';

const Home = () => {
  const [filtered, setFiltered] = useState(false);
  const [categories, setCategories] = useState([]);
  const [items, setItems] = useState({});
  const [filteredCategories, setFilteredCategories] = useState([]);
  const [filteredItems, setFilteredItems] = useState({});

  useEffect(() => {
    if (!filtered) {
      getData('categories', setCategories);
      getData('items', docs => {
        setItems(groupByKey(docs, 'category_id'));
      });
    }
  }, [filtered]);

  const handleSearchOnChange = e => {
    const value = e.target.value.toLowerCase();

    if (filtered && !value) {
      setFiltered(false);
    }

    if (value) {
      const auxItems = groupByKey(
        Object.values(items)
          .flat()
          .filter(item => item.name.toLowerCase().includes(value)),
        'category_id'
      );

      const auxCategories = categories.filter(category => {
        const itemsMatch = Object.keys(auxItems).includes(category.id);
        const valueMatch = category.name.toLowerCase().includes(value);
        return itemsMatch || valueMatch;
      });

      setFilteredCategories(auxCategories);
      setFilteredItems(auxItems);

      if (!filtered) {
        setFiltered(true);
      }
    }
  };

  return (
    <>
      <SearchBar onChange={handleSearchOnChange} />
      {filtered ? (
        filteredCategories.length > 0 ? (
          filteredCategories.map(category => (
            <CategoryAccordion
              key={category.id}
              category={category}
              items={filteredItems[category.id] || []}
            />
          ))
        ) : (
          <Text text='No categories or items were found' />
        )
      ) : (
        categories.length > 0 &&
        categories.map(category => (
          <CategoryAccordion
            key={category.id}
            category={category}
            items={items[category.id] || []}
          />
        ))
      )}
    </>
  );
};

export default Home;
