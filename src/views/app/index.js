import React from 'react';
import { Route, Routes } from 'react-router-dom';

import { APP_PATHS } from '../../libs/constants';
import { MainLayout } from '../../components/ui/layouts';
import { Loader } from '../../components';

const ViewHome = React.lazy(() => import('./Home'));
const ViewCreate = React.lazy(() => import('./Create'));
const ViewFavorites = React.lazy(() => import('./Favorites'));
const ViewNoMatch = React.lazy(() => import('../NoMatch'));

const App = () => {
  return (
    <MainLayout>
      <Loader>
        <Routes>
          <Route index element={<ViewHome />} />
          <Route path={APP_PATHS.create} element={<ViewCreate />} />
          <Route path={APP_PATHS.favorites} element={<ViewFavorites />} />
          <Route path={APP_PATHS.all} element={<ViewNoMatch />} />
        </Routes>
      </Loader>
    </MainLayout>
  );
};

export default App;
