import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import toast from 'react-hot-toast';
import { Navigate, useNavigate } from 'react-router-dom';

import { AuthForm } from '../components/ui/forms';
import { userSelector, clearState } from '../features/user/slice';
import { signinUser } from '../features/user/api';
import { APP_PATHS } from '../libs/constants';

const SignIn = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isSuccess, isError, errorMessage, auth } = useSelector(userSelector);

  useEffect(() => {
    return () => {
      dispatch(clearState());
    };
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      toast.error(errorMessage);
      dispatch(clearState());
    }
    if (isSuccess) {
      dispatch(clearState());
      navigate(APP_PATHS.root);
    }
  }, [dispatch, errorMessage, isError, isSuccess, navigate]);

  const handleOnSignIn = data => {
    dispatch(signinUser(data));
  };

  console.log(auth);

  return (
    <>
      {auth ? (
        <Navigate replace to={APP_PATHS.root} />
      ) : (
        <AuthForm
          title='Sign In'
          description='To sign in please enter your email and password'
          linkTo={APP_PATHS.signup}
          linkText='Sign Up'
          footerDescription='Not signed up yet?'
          onSubmit={handleOnSignIn}
        />
      )}
    </>
  );
};

export default SignIn;
