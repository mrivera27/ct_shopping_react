import React, { StrictMode } from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom/client';
import { Toaster } from 'react-hot-toast';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { Loader } from './components';
import { APP_PATHS } from './libs/constants';
import store from './features/store';
import './styles/index.scss';

const ViewApp = React.lazy(() => import('./views/app'));
const ViewNoMatch = React.lazy(() => import('./views/NoMatch'));
const ViewSignIn = React.lazy(() => import('./views/SignIn'));
const ViewSignUp = React.lazy(() => import('./views/SignUp'));

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <StrictMode>
    <Provider store={store}>
      <Toaster />
      <Loader>
        <BrowserRouter>
          <Routes>
            <Route path={APP_PATHS.all} element={<ViewApp />} />
            <Route path={APP_PATHS.signin} element={<ViewSignIn />} />
            <Route path={APP_PATHS.signup} element={<ViewSignUp />} />
            <Route path='*' element={<ViewNoMatch />} />
          </Routes>
        </BrowserRouter>
      </Loader>
    </Provider>
  </StrictMode>
);

document.getElementById('root');
