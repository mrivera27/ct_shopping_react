export const groupByKey = (_data, _key) => {
  return _data.reduce((result, next) => {
    const key = next[_key];
    result[key] = result[key]?.length ? [...result[key], next] : [next];
    return result;
  }, {});
};
