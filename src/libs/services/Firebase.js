import { initializeApp } from 'firebase/app';
import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  getFirestore,
  orderBy,
  query,
  setDoc,
  updateDoc,
  where,
} from 'firebase/firestore';
import {
  createUserWithEmailAndPassword,
  getAuth,
  signInWithEmailAndPassword,
  signOut,
} from 'firebase/auth';
import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage';

import { FIREBASE_CONFIG } from '../constants';

class Firebase {
  constructor() {
    const app = initializeApp(FIREBASE_CONFIG);

    this.auth = getAuth(app);
    this.db = getFirestore(app);
    this.storage = getStorage(app);
  }

  async signin(email, password) {
    const user = await signInWithEmailAndPassword(this.auth, email, password);
    return user;
  }

  async signup(email, password) {
    let response = await createUserWithEmailAndPassword(
      this.auth,
      email,
      password
    );

    return response;
  }

  async signout() {
    return await signOut(this.auth);
  }

  async getAll(path) {
    const q = query(collection(this.db, path), orderBy('name'));
    const snapshot = await getDocs(q);
    return snapshot;
  }

  async get(path, id) {
    const snapshot = await getDoc(doc(this.db, path, id));
    return snapshot.exists() ? snapshot.doc : null;
  }

  async getBy(path, property, value) {
    const q = query(collection(this.db, path), where(property, '==', value));
    const snapshot = await getDocs(q);
    return snapshot;
  }

  async set(path, id, data) {
    await setDoc(doc(this.db, path, id), data);
  }

  async add(path, data) {
    await addDoc(collection(this.db, path), data);
  }

  async update(path, id, data) {
    const ref = doc(this.db, path, id);
    await updateDoc(ref, data);
  }

  async remove(path, id) {
    const ref = doc(this.db, path, id);
    return await deleteDoc(ref);
  }

  async uploadFile(file, path) {
    const storageRef = ref(this.storage, `${path}/${file.name}`);
    await uploadBytes(storageRef, file);
    return getDownloadURL(storageRef);
  }
}

export default new Firebase();
