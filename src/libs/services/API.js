import Firebase from './Firebase';

export const getData = (path, callBack) => {
  Firebase.getAll(path).then(snapshot => {
    const docs = [];

    snapshot.forEach(doc => {
      docs.push({
        id: doc.id,
        ...doc.data(),
      });
    });

    callBack(docs);
  });
};
