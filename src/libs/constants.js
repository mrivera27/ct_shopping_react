// CONFIG
export const FIREBASE_CONFIG = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID,
};

// GENERAL
export const APP_PATHS = {
  root: '/',
  create: '/create',
  favorites: '/favorites',
  results: '/results',
  signin: '/signin',
  signup: '/signup',
  all: '/*',
};

export const BREAKPOINTS = {
  mobile: 0,
  mobileLg: 450,
  tablet: 768,
  tabletLg: 881,
  desktop: 1024,
};

// AUTH
export const AUTH_COOKIE_KEY = '__tk_auth';
export const USER_STORAGE_KEY = '__current_user';
